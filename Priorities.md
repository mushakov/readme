## 2022-09-28
1. Onboarding help for Amanda and Matthew
1. PI meeting updates
1. Figure out financials for Service Desk proposal
1. Nail down timeline and iteration path for OKRs
1. UX roadmap for Product Planning
1. Prepare CDFs for my team and have my own review!


## 2022-09-19
1. Y - Onboarding help for Amanda and Matthew
1. Y - Submit all pending interview scorecards
1. Y - Customer calls
1. Y - Continue to decompose OKR requirements and define a path forward.
1. Y - Figure out staffing needs for Service Desk
1. Not part of original plan
   1. PI updates

## 2022-09-12
1. Y - Complete 15.5 planning
1. Y -Release posts
1. Y - Cover for Team Planning PM
1. Y - Manage GMP interviews
1. Y - Tie all loose ends to onboard two new PMs :tada:
1. Y - 360 feedback next steps
1. Not part of original plan
   1. Decompose OKR requirements for dogfooding
   1. Decomponse Service Desk requirements for dogfooding

## 2022-09-06
1. Y - Help with interviews for Manage GMP 
1. Y - Various activities to form the Certify group as separate for Prod Plan
1. Y - Complete onboarding issue for Certify PM
1. N - CDF and 360 feedback next steps
1. Y - Customer calls

## 2022-08-15
1. Y - Finalize 15.4 planning for Product Planning
1. Y - Prepare for covering the dev section Kick-Off call
1. Y - PI review meeting
1. Y - Continue transition issues for Product Planning and Certify
1. Y - Help with interviews for Manage GMP 
1. N - Competitive analysis for Plan market up-and-comers

## 2022-08-08
1. N - Competitive analysis for Plan market up-and-comers.
1. Y - Start transition issues for Product Planning and Certify
1. Y - Cover for Grant and Gabe while they are OOO
1. Y - Attend opp canvas review for Statuses :D https://docs.google.com/document/d/1MmaeawFxTQRRGw0ZQAhkSUlQuXQIiQoMggXcIWu1sNw/edit 
1. Y - Preliminary 15.4 planning for Product Planning
1. Not part of priorities originally documented
   - PI updates for Plan Stage, Product Planning and Certify 

## 2022-08-01
1. Y - Continue to help triage and route Tasks feedback
1. Y - Customer calls and document in DoveTail
1. Y - Certify PM hiring
1. N - Preliminary 15.4 planning for Product Planning
1. Y - Review opp canvas and canvas lite for Project Management
1. Y - Cover for Grant and Gabe while they are OOO
1. Not part of priorities originally documented
   - Sick on Friday

## 2022-07-25
1. Y - Continue to help triage and route Tasks feedback 
1. Y - Work Items AMAs
1. Y - Document and deliver CDF feedback
1. Y - Help with interviews
1. Y - Certify PM hiring 

## 2022-07-18
1. Y - Continue to help triage and route Tasks feedback 
1. Y - PI updates and review
1. N - Direction page updates
1. Y/N - Work on CDFs

## 2022-07-11
1. Y - 15.3 plan for product planning.
1. Y - Help interview for open roles 
1. N - PI Updates and review
1.  Not part of priorities originally documented
  - Triage Tasks feedback

## 2022-07-06
1. Y - Finalize agreement on Alliances/Integrations division of roles.
1. Y - Help interview for open roles 
1. Y - Refine ideas for 15.3 for product planning 

## 2022-06-27
1. Y - Help get Tasks over the finish line.
1. Y - Complete a proposal for the `Complete` maturity date for Portfolio Management https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/105759 .
1. Y - Direction page updates for Ecosystem.
1. Y - Continue to explore PM and Alliances division of reponsibilities. Set up meeting to discuss.
1. Y - Meeting with customer to get Plan feedback.


## 2022-06-20
1. YN - Direction page updates for Plan and Ecosystem
1. Y - Complete PI Updates for Product Planning
1. Y - Contribute to Executive persona research
1. N - Complete a proposal for the `Complete` maturity date for Portfolio Management https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/105759 
1. Y - Friends and Family Day on Friday :D
1. Not part of priorities originally documented
   - Spent significant time on MRs to transition Certify.
   - PI updates for Certify
   - Help get Tasks over the finish line

## 2022-06-13
1. Y - Executive persona interviews
1. Y - More LifeLabs Manager Training
1. Y - 15.2 Release Planning for Product Planning
1. Y - Clean up cross group relationships issue - https://gitlab.com/gitlab-org/gitlab/-/issues/205155 
1. N - Complete a proposal for the `Complete` maturity date for Portfolio Management https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/105759 
1. Not part of priorities originally documented
   - Start PI updates for Product Planning 
   - PI update reviews for Plan and Ecosystem
   - Competitive analysis for Tasks

# 2022-06-06
1. Y - Interview for open roles - SEM Plan, Prod Plan PM.
1. Y - Review content from the Plan Enablement working group
1. Y - LifeLabs Manager Training
1. Y - CDF review prep
1. Y -  Coverage for Foundations PM as needed while Christen is OOO
1. Clean up various feature proposals for Product Planning and get them ready for more validation
   - Y - Dates and rollups - https://gitlab.com/gitlab-org/gitlab/-/issues/339758
   - N - Cross group relationships - https://gitlab.com/gitlab-org/gitlab/-/issues/205155 
   - Y - Weight rollups. https://gitlab.com/gitlab-org/gitlab/-/issues/339760 
1. Not part of priorities originally documented
   - Review 2 opportunity canvases
   - Career planning :)
   - Product direction showcase for Plan
   - Maturity date adjustments for various categories in Plan


## 2022-05-31
1. Y - Rebuild a pipeline for Product Planning PM role.
1. N - Review content from the Plan Enablement working group.
1. Y - Prepare for ARR Drivers Sync.
1. Y - Schedule more internal sessions for [Executive persona](https://gitlab.com/gitlab-org/ux-research/-/issues/1457) research.
1. Y - Catch up on LifeLabs Manager training missed session.

## 2022-05-23
1. Y - Vacation catch-up!

## 2022-05-16
1. Y - GMP Interviews
1. Y - Vacation - https://gitlab.com/gitlab-com/Product/-/issues/4209
1. Y - CDF Reviews

## 2022-05-09
1. Y - Finalize Product Planning PM hiring next steps 
1. N - CDF reviews this week!
1. Y - [Executive persona](https://gitlab.com/gitlab-org/ux-research/-/issues/1457) research
1. Y - Determine next steps for [PIE role](https://about.gitlab.com/job-families/product/partner-integration-engineering/)
1. Y - Wrap up 15.0 release
1. N - Scope 15.1 for Product Planning before I take next week off!
1. Items completed but not included in initial report
    1. Y - Product Planning PI Updates
    1. Y - Last minute customer call
    1. Y - GMP interview and scorecard
    1. Y - LifeLabs manager training
